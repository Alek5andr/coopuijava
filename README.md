# Coop
https://www.cooppank.ee/en/leasing

Automating "Leasing calculator"

Tech used:
* Java
* Selenide(Selenium) as web UI automation framework
* Cucumber as Gherkin framework
* Log4j2 as logger
* JUnit as assertion as test runner and assertion framework
* Maven as project builder and launcher

Test materials: test suite and report are located in Materials/ folder.

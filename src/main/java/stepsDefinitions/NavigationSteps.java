package stepsDefinitions;

import io.cucumber.java8.En;
import pageObjects.Navigation;
import services.ErrorThrower;

public class NavigationSteps implements En {
    ErrorThrower errorThrower = new ErrorThrower();
    Navigation navigation = new Navigation();

    public NavigationSteps() {
        When("navigation to {string} page is made", (String page) -> {
            switch (page) {
                case "Leasing":
                    navigation.navigateToUrl("https://www.cooppank.ee/autoliising");
                    break;
                default:
                    errorThrower.throwIllegalArgumentError("No such page is implemented: " + page);
            }
        });
    }
}

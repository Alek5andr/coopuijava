package stepsDefinitions;

import io.cucumber.java8.En;
import org.junit.Assert;
import pageObjects.LeasingPage;
import services.ErrorThrower;

public class LeasingPageSteps implements En {
    private ErrorThrower errorThrower = new ErrorThrower();
    private LeasingPage leasingPage = new LeasingPage();

    public LeasingPageSteps() {
        When("leasing price {int} of vehicle is set", (Integer int1) -> {
            Assert.assertTrue(leasingPage.setValueToVehiclePriceField(int1));
        });

        When("leasing period {int} of vehicle is set", (Integer int1) -> {
            Assert.assertTrue(leasingPage.setValueToVehiclePeriodField(int1));
        });

        When("initial payment {int} in {string} is set", (Integer int1, String units) -> {
            switch (units) {
                case "percentages":
                    leasingPage.setInitialPaymentPercentageValue(int1);
                    break;
                default:
                    this.throwNoSuchUnitsImplementedError(units);
            }
        });

        When("last payment amount {int} in {string} is set", (Integer int1, String units) -> {
            switch (units) {
                case "percentages":
                    leasingPage.setLastPaymentPercentageValue(int1);
                    break;
                default:
                    this.throwNoSuchUnitsImplementedError(units);
            }
        });

        When("{string} radio button is selected", (String leasingType) -> {
            Assert.assertTrue(leasingPage.selectLeasingTypeRadioButtonByText(leasingType));
        });

        Then("monthly payment is {double}", (Double double1) -> {
            Assert.assertEquals("Monthly payment assertion failed:", String.valueOf(double1), leasingPage.getMonthlyPayment());
        });
    }

    private void throwNoSuchUnitsImplementedError(String units) {
        errorThrower.throwIllegalArgumentError("No such units are implemented: " + units);
    }
}

package pageObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import services.Log;

import static com.codeborne.selenide.Selectors.*;

class Field extends Shared {
    void setValueToElement(SelenideElement element, String value) {
        Log.info("Setting value: " + value + " - to element: " + element.getSearchCriteria());

        element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        element.sendKeys(value);
        element.pressTab();
    }

    Boolean clickParentElementAndSetValueToChildElement(SelenideElement parentElement, String childElementCssSelector, String value) {
        if (super.clickElement(parentElement)) {
            this.setValueToElement(parentElement.find(byCssSelector(childElementCssSelector)), value);
            return true;
        }
        return false;
    }
    Boolean clickParentElementAndSetValueToInputField(SelenideElement element, String value) {
        return this.clickParentElementAndSetValueToChildElement(element, super.getInput(), value);
    }
}

package pageObjects;

import com.codeborne.selenide.Selectors;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import services.ErrorThrower;
import services.Log;

import static com.codeborne.selenide.Selenide.$;

public class LeasingPage extends Shared {
    private ErrorThrower errorThrower = new ErrorThrower();
    private Field field = new Field();

    private SelenideElement getInitialPaymentPercentageFieldElement() {
        return $(By.name("esmaneprots"));
    }

    private SelenideElement getLastPaymentPercentageFieldElement() {
        return $(By.name("jaakvprots"));
    }

    private SelenideElement getMonthlyPaymentElement() {
        return $(Selectors.byCssSelector("p[class^='c-form-field__summary']>span[class='value']"));
    }

    public Boolean setValueToVehiclePriceField(int value) {
        Log.info("Setting value to vehicle leasing price field: " + value);
        return this.getParentElementAndClickItAndSetValueToIt("Ostuhind", value);
    }

    public Boolean setValueToVehiclePeriodField(int value) {
        Log.info("Setting value to vehicle leasing period field: " + value);
        return this.getParentElementAndClickItAndSetValueToIt("Periood", value);
    }

    public void setInitialPaymentPercentageValue(int value) {
        Log.info("Setting initial payment percentage value: " + value);
        field.setValueToElement(this.getInitialPaymentPercentageFieldElement(), String.valueOf(value));
    }

    public void setLastPaymentPercentageValue(int value) {
        Log.info("Setting last payment percentage value: " + value);
        field.setValueToElement(this.getLastPaymentPercentageFieldElement(), String.valueOf(value));
    }

    public Boolean selectLeasingTypeRadioButtonByText(String text) {
        Log.info("Selecting radio button by leasing type: " + text);
        return super.clickElement(super.getFormFieldRadioButtonElementByText(text));
    }

    public String getMonthlyPayment() {
        Log.info("Getting monthly payment value...");
        return this.getMonthlyPaymentElement().getOwnText();
    }

    private Boolean getParentElementAndClickItAndSetValueToIt(String text, int value) {
        SelenideElement element = super.getElementParentByLabelTagText(
                super.getSliderInputFieldElements(),
                text);
        if (element.exists()) {
            field.clickParentElementAndSetValueToInputField(element, String.valueOf(value));
            return true;
        }
        return false;
    }
}

package pageObjects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import services.Log;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

class Shared {
    private SelenideElement pageContainer() {
        return $(byId("wrap"));
    }

    String getH1() {
        return "h1";
    }

    String getH2() {
        return "h2";
    }

    String getButton() {
        return "button";
    }

    String getInput() {
        return "input";
    }

    String getLabel() {
        return "label";
    }

    String getClassValueSelector() {
        return "value";
    }

    String getSliderInputFieldSelector() {
        return "c-slider-input__input";
    }

    String getClassText() {
        return "text";
    }

    ElementsCollection getSliderInputFieldElements() {
        return $$(byClassName(this.getSliderInputFieldSelector()));
    }

    SelenideElement getElementParentByLabelTagText(ElementsCollection elements, String text) {
        for (SelenideElement element : elements) {
            Log.info("Collecting label elements of parent element");

            ElementsCollection labelElements = element.findAll(byTagName(this.getLabel())).filter(Condition.exactText(text));
            if (labelElements.size() > 0) {
                Log.info("  labelElements size is: " + labelElements.size());
                Log.info("  Returning first label element\'s parent element.");
                return labelElements.first().parent();}
        }
        throw new IllegalArgumentException("There wre no labelElements.");
    }

    ElementsCollection getFormFieldRadioButtonElements() {
        return $$(byClassName("c-form-field__radio"));
    }

    SelenideElement getFormFieldRadioButtonElementByText(String text) {
        for (SelenideElement radioButtonElement : this.getFormFieldRadioButtonElements()) {
            if (text.equals(radioButtonElement.find(byClassName(this.getClassText())).getOwnText())) {
                return radioButtonElement.parent();
            }
        }
        throw new IllegalArgumentException("Radio button is not found with text: " + text);
    }

    Boolean clickElement(SelenideElement element) {
        Log.info("Clicking on element with selector: " + element.getSearchCriteria());
        boolean isExist = element.exists();

        if (isExist) {
            if(!element.isDisplayed()) element.scrollTo();
            element.click();
        }
        return isExist;
    }

}

package pageObjects;

import static com.codeborne.selenide.Selenide.open;

public class Navigation extends Shared {
    public void navigateToUrl(String url) {
        open(url);
    }
}

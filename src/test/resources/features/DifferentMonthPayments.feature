Feature: Find out financial and operational monthly payments
  & confirm that financial is higher than operational

  Scenario Outline: Monthly payments & difference of payments
    When navigation to "Leasing" page is made
    When leasing price <price> of vehicle is set
    And leasing period <period> of vehicle is set
    And initial payment <initPayment> in <initPaymentUnits> is set
    And last payment amount <lastPayment> in <lastPaymentUnits> is set
    Then monthly payment is <finMonthlyPayment>
    When <leasingType> radio button is selected
    Then monthly payment is <opMonthlyPayment>

    Examples:
      | price | period | initPayment | initPaymentUnits | lastPayment | lastPaymentUnits | finMonthlyPayment | leasingType   | opMonthlyPayment |
      | 45000 | 46     | 20          | "percentages"    | 5           | "percentages"    | 811.33            | "Kasutusrent" | 784.63           |